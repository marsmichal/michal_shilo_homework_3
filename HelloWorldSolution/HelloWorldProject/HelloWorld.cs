﻿using System;

class MainClass
{
	public static void Main()
	{
		Console.WriteLine("Claudia is excited to get to class, but on her way to the university a tree falls on her head." +
		                  "Press 1 if she should call an ambulance. Press 2 if she should continue to class.");
		char pressedKey = Console.ReadKey().KeyChar;
		Console.WriteLine();

		if (pressedKey == '1')
		{
			Console.WriteLine("You pressed 1. Claudia calls an ambulance.");
		}
	if (pressedKey == '2')
		{
			Console.WriteLine("You pressed 2. Claudia will stumble onto the subway, leaving a trail of blood behind her...");
		}

	}					
}

